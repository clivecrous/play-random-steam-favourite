#include <stdlib.h>

#include <sstream>
#include <vector>

#include <windows.h>

#include "favourites.h"

void run_steam_app_id( int app_id )
{
	std::stringstream command;
	command << "start steam://run/" << app_id;
	system( command.str().c_str() );
}

int main( int argc, char *argv[] )
{
	srand( GetTickCount() ); // seed random number generator

	try
	{
		SteamIDs favourites_list( favourites() );
		if ( favourites_list.size() == 0 ) {
		  MessageBox( NULL, "No steam favourites found :(", "Error", MB_OK | MB_ICONEXCLAMATION );
		} else {
	          run_steam_app_id( favourites_list[ rand() % favourites_list.size() ] );
		}
		return 0;
	}
	catch( const char * error_message )
	{
		MessageBox( NULL, error_message, "Error", MB_OK | MB_ICONEXCLAMATION );
	}
	catch( ... )
	{
		MessageBox( NULL, "Oh dear :(", "Unknown Error", MB_OK | MB_ICONERROR );
	}
	return -1;
};
