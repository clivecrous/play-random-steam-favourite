#include <sstream>
#include <vector>

#include <windows.h>

#include "favourites.h"

unsigned int is_an_uptodate_favourite( unsigned int app_id )
{

	// Open root registry key for this application

	HKEY registry_key;
	{
		std::stringstream registry_key_location;

		registry_key_location
			<< "Software\\Valve\\Steam\\Apps\\"
			<< app_id;

		LONG openkey_result =
			RegOpenKeyEx(
				HKEY_CURRENT_USER,
				registry_key_location.str().c_str(),
				0,
				KEY_QUERY_VALUE,
				&registry_key );

		if ( openkey_result != ERROR_SUCCESS )
			throw "Unable to open steam application's registry key";
	}

	// Read application registry installed value

	DWORD is_installed;
	DWORD is_installed_type;
	DWORD is_installed_size( sizeof(DWORD) );
	{
		LONG queryvalue_result =
			RegQueryValueEx(
				registry_key,
				"Installed",
				NULL,
				&is_installed_type,
				(BYTE *)&is_installed,
				&is_installed_size );

		if ( queryvalue_result != ERROR_SUCCESS )
			return 0; // Not an error, some apps have no category
	}

	// Return zero if this application is not installed

	if ( is_installed_type != REG_DWORD ||
	     is_installed_size != sizeof(DWORD) ||
	     is_installed != 1 )
		return 0;

	// Read application registry updated value

	DWORD is_updated;
	DWORD is_updated_type;
	DWORD is_updated_size( sizeof(DWORD) );
	{
		LONG queryvalue_result =
			RegQueryValueEx(
				registry_key,
				"UpToDate",
				NULL,
				&is_updated_type,
				(BYTE *)&is_updated,
				&is_updated_size );

		if ( queryvalue_result != ERROR_SUCCESS )
			return 0; // Not an error, some apps have no category
	}

	// Return zero if this application is not updated

	if ( is_updated_type != REG_DWORD ||
	     is_updated_size != sizeof(DWORD) ||
	     is_updated != 1 )
		return 0;

	// Read application registry category value

	char favourite_string[ 9 ];
	DWORD favourite_string_type;
	DWORD favourite_string_size( 9 );
	{
		LONG queryvalue_result =
			RegQueryValueEx(
				registry_key,
				"Category",
				NULL,
				&favourite_string_type,
				(BYTE *)&favourite_string,
				&favourite_string_size );

		if ( queryvalue_result != ERROR_SUCCESS )
			return 0; // Not an error, some apps have no category
	}

	// Return zero if this key is not a favourite

	if ( favourite_string_type != REG_SZ ||
	     favourite_string_size != 9 ||
	     strcmp( favourite_string, "favorite" ) )
		return 0;

	// It must be a favourite :)

	return 1;
}

SteamIDs favourites( void )
{
	// Open root registry key for all applications

	HKEY registry_key;
	{
		std::stringstream registry_key_location;

		registry_key_location
			<< "Software\\Valve\\Steam\\Apps";

		LONG openkey_result =
			RegOpenKeyEx(
				HKEY_CURRENT_USER,
				registry_key_location.str().c_str(),
				0,
				KEY_ENUMERATE_SUB_KEYS,
				&registry_key );

		if ( openkey_result != ERROR_SUCCESS )
			throw "Unable to open steam's application list registry key";
	}

	// Itterate through application IDs testing for favourites

	SteamIDs favourites_list;

	{
		char key_name[ 32 ];
		DWORD key_name_size; // at time of writing 5 + 1 should be enough;
		
		for ( unsigned int subkey_index(0);;subkey_index++ )
		{
			key_name_size = 32;
			LONG enumkey_result =
				RegEnumKeyEx(
					registry_key,
					subkey_index,
					(CHAR *)&key_name,
					&key_name_size,
					NULL, NULL, NULL, NULL );

			if ( enumkey_result == ERROR_SUCCESS )
			{
				int app_id( atoi( key_name ) );
				if ( app_id > 0 && app_id != 1000 && is_an_uptodate_favourite( app_id ) )
					favourites_list.push_back( app_id );
			}
			else
			{
				if ( enumkey_result == ERROR_MORE_DATA ) continue;
				if ( enumkey_result == ERROR_NO_MORE_ITEMS ) break;
				throw "Unable to enumerate steam application registry list";
			}


		}
	}

	return favourites_list;
}
